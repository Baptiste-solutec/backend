# Récupération de l'IP et port de Nexus
ARG REPOSITORY

# Image de base
FROM ${REPOSITORY}/php:cert

# Récupération du port provenant du fichier d'environnement
# Cet argument est fourni par Docker Compose car Dockerfile ignore .env
# Fonctionne au run time
ARG PORT
# Définition d'une variable d'environnement  pour qu'il soit fixé au build time
# et ainsi fonctionner dans la commande à lancer
ENV PORT ${PORT}

# Ajout de l'app dans le container
WORKDIR /app
COPY ./app /app
RUN composer install

# Lancement de l'application
CMD php bin/console server:run 0.0.0.0:$PORT