# Installation des dépendances pour contruire l'application
```
sudo apt install composer
sudo apt install php7.2-gd php7.2-intl php7.2-xsl
```

# Commandes du starter Symfony

## Ne pas oublier de changer le port d'écoute dans le .env

### Création d'une application Symfony
```
./create-symfony-app.sh
```

### Lancement du container en forcent le build
```
docker-compose up -d --build
```

### Lancement du container
```
docker-compose up -d
```

### Lancer une commande dans le container via un terminal (Linux)
```
docker exec -i [container_name] [command]
```
### Exemple
```
docker exec -i mon_container composer install
```

# Mise en preprod et prod
Afin de mettre l'application en preprod et prod il est recommandé d'utiliser Nginx en temps que serveur http.
L'application doit être configurer pour de la production (ajout de fichier de conf pour Nginx, désactiver le debug/mode dev...) et le fichier Dockerfile.production rempli en conséquence. Le port par défaut de Nginx et le port 80 et non le port de votre application en mode dev. Pensez donc à modifier le fichier `.env`.